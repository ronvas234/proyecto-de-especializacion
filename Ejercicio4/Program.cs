﻿using System;

namespace Ejercicio4
{
    class Program
    {
        static void Main(string[] args)
        {
            int num;
            Console.WriteLine("Ingrese Número del 1 al 7:");
            var estado = validarnumero(Console.ReadLine(),out num);
            if (estado)
            {
                switch (num)
                {
                    case 1:
                        Console.WriteLine("Día Lunes");
                        break;
                    case 2:
                        Console.WriteLine("Día Martes");
                        break;
                    case 3:
                        Console.WriteLine("Día Miércoles");
                        break;
                    case 4:
                        Console.WriteLine("Día Jueves");
                        break;
                    case 5:
                        Console.WriteLine("Día Viernes");
                        break;
                    case 6:
                        Console.WriteLine("Día Sabado");
                        break;
                    case 7:
                        Console.WriteLine("Día Domingo");
                        break;
                    default:
                        Console.WriteLine(string.Format("El número debe estar en el rango del 1 al 7: {0}",num));
                        break;
                }
            }
            Console.Read();
        }
        private static bool validarnumero(string variable, out int salida)
        {
            var estado = true;
            if (!int.TryParse(variable, out salida))
            {
                Console.WriteLine(string.Format("Entrada Invalida {0}", variable));
                estado = false;
            }
            return estado;
        }
    }
}
