﻿using System;

namespace Ejercicio5
{
    class Program
    {
        public static double num;
        static void Main(string[] args)
        {
            Console.WriteLine("Ingrese Número del 1 al 20:");
            var estado = validarnumero(Console.ReadLine(), out num);
            if (estado)
            {
                operaciones();
            }
            Console.Read();
        }
        private static bool validarnumero(string variable, out double salida)
        {
            var estado = true;
            if (!double.TryParse(variable, out salida))
            {
                Console.WriteLine(string.Format("Entrada Invalida {0}", variable));
                estado = false;
            }
            return estado;
        }
        private static void operaciones()
        {
            int r=0;
            try
            {
                if(num>=19 && num <= 20)
                {
                    r = 1;
                }
                if(num>=16 && num <= 18)
                {
                    r = 2;
                }
                if(num>=14 && num <= 15)
                {
                    r = 3;
                }
                if (num >= 12 && num <= 13)
                {
                    r = 4;
                }
                if (num >= 1 && num <= 11)
                {
                    r = 5;
                }
                switch (r)
                {
                    case 1:
                        Console.WriteLine("CALIFICACIÓN SOBRESALIENTE");
                        break;
                    case 2:
                        Console.WriteLine("CALIFICACIÓN MUY BUENA");
                        break;
                    case 3:
                        Console.WriteLine("CALIFICACIÓN BUENA");
                        break;
                    case 4:
                        Console.WriteLine("CALIFICACIÓN REGULAR");
                        break;
                    case 5:
                        Console.WriteLine("CALIFICACIÓN INSUFICIENTE");
                        break;
                    default:
                        Console.WriteLine("CALIFICACIÓN MUY MALO");
                        break;
                }
            }
            catch (Exception ex)
            {

                Console.WriteLine(ex);
            }
        }
    }
}
