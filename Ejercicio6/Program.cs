﻿using System;

namespace Ejercicio6
{
    class Program
    {
        public static double num;
        static void Main(string[] args)
        {
            double resultado;
            Console.WriteLine("Digite un Número:");
            var estado = validarnumero(Console.ReadLine(), out num);
            if (estado)
            {
                for(var i=1; i <= 12; i++)
                {
                    resultado = num * i;
                    Console.WriteLine(string.Format("Resultado: {0}",resultado));
                }
            }
            Console.Read();
        }
        private static bool validarnumero(string variable, out double salida)
        {
            var estado = true;
            if (!double.TryParse(variable, out salida))
            {
                Console.WriteLine(string.Format("Entrada Invalida {0}", variable));
                estado = false;
            }
            return estado;
        }
    }
}
