﻿using System;

namespace Ejercicio3
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Ingrese el Nombre:");
            var nombre=Console.ReadLine();
            if (nombre.ToLower().StartsWith("david"))
            {
                Console.WriteLine("Hola");
            }
            else
            {
                Console.WriteLine("No te conozco");
            }
            Console.Read();
        }
    }
}
