﻿using System;

namespace Ejercicio1
{
    class Program
    {
        public static int numero1;
        public static int numero2;
        static void Main(string[] args)
        {
            var estado=leerdatos();
            if (estado)
            {
                operaciones();
            }
            Console.Read();
        }

        private static bool  leerdatos()
        {
            bool estado;
            Console.WriteLine("Introducir el Número 1:");
            estado= validarnumero(Console.ReadLine(),out numero1);
            if (estado)
            {
                Console.WriteLine("Introducir el Número 2:");
                estado = validarnumero(Console.ReadLine(), out numero2);
            }
            return estado;
        }
        private static bool validarnumero(string variable,out int salida)
        {
            var estado = true;
            if(!int.TryParse(variable, out salida))
            {
                Console.WriteLine(string.Format("Entrada Invalida {0}",variable));
                estado = false;
            }
            return estado;
        }
        private static void operaciones()
        {
            double resultado;
            try
            {
                resultado = numero1 + numero2;
                Console.WriteLine(string.Format("La suma es: {0}",resultado));
                resultado = numero1 - numero2;
                Console.WriteLine(string.Format("La resta es: {0}", resultado));
                resultado = numero1 * numero2;
                Console.WriteLine(string.Format("La multiplicación es: {0}", resultado));
                if (numero2 > 0)
                {
                    resultado = numero1 / numero2;
                    Console.WriteLine(string.Format("La división es: {0}", resultado));
                }
                else
                {
                    Console.WriteLine("El divisor debe ser mayor a cero");
                }
            }
            catch (DivideByZeroException ex)
            {

                Console.WriteLine(ex);
            }
            catch (Exception ex)
            {

                Console.WriteLine(ex);
            }
        }
    }
}
