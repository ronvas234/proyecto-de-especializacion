﻿using System;

namespace Ejercicio8
{
    class Program
    {
        public static double num;
        static void Main(string[] args)
        {
            double resultado=0;
            int x;
            int i = 1;
            Console.WriteLine("Ingrese un Número Positivo:");
            var estado = validarnumero(Console.ReadLine(), out num);
            if (estado)
            {
                if (num < 0)
                {
                    Console.WriteLine(string.Format("Debe ingresar un número positivo: {0}",num));
                }
                else
                {
                    resultado = Math.Log10(num);
                    x = ((int) resultado)+1;
                    Console.WriteLine(string.Format("La cantidad de cifra es: {0}", x));
                }
            }
            Console.Read();
        }
        private static bool validarnumero(string variable, out double salida)
        {
            var estado = true;
            if (!double.TryParse(variable, out salida))
            {
                Console.WriteLine(string.Format("Entrada Invalida {0}", variable));
                estado = false;
            }
            return estado;
        }
    }
}
