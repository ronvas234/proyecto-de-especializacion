﻿using System;

namespace Ejercicio2
{
    class Program
    {
        public static int canthab;
        public static int cantpap;
        public static int cantbeb;
        static void Main(string[] args)
        {
            var estado = leerdatos();
            if (estado)
            {
                operaciones();
            }
            Console.Read();
        }
        private static bool leerdatos()
        {
            bool estado;
            Console.WriteLine("Ingrese la Cantidad de Hamburguesa:");
            estado = validarnumero(Console.ReadLine(), out canthab);
            if (estado)
            {
                Console.WriteLine("Ingrese la Cantidad de Papas:");
                estado = validarnumero(Console.ReadLine(), out cantpap);
                if (estado)
                {
                    Console.WriteLine("Ingrese la Cantidad de Bebidas:");
                    estado = validarnumero(Console.ReadLine(), out cantbeb);
                }
            }
            return estado;
        }
        private static bool validarnumero(string variable, out int salida)
        {
            var estado = true;
            if (!int.TryParse(variable, out salida))
            {
                Console.WriteLine(string.Format("Entrada Invalida {0}", variable));
                estado = false;
            }
            return estado;
        }
        private static void operaciones()
        {
            double resultado;
            double totalresultado=0;
            try
            {
                resultado = canthab * 2;
                Console.WriteLine(string.Format("Hamburguesa {0}*{1}={2}", canthab, 2, resultado));
                totalresultado += resultado;
                resultado = cantbeb * 1.2;
                Console.WriteLine(string.Format("Bebida {0}*{1}={2}", cantbeb, 1.2, resultado));
                totalresultado += resultado;
                resultado = cantpap * 0.8;
                Console.WriteLine(string.Format("Papa {0}*{1}={2}", cantpap, 0.8, resultado));
                totalresultado += resultado;
                Console.WriteLine(string.Format("El total a pagar es: {0}", totalresultado));
            }
            catch (Exception ex)
            {

                Console.WriteLine(ex);
            }
        }
    }
}
